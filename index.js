// console.log('Hello, World');

let number = Number(prompt('Enter number:'));
console.log(`The number you provided is ${number}.`)

for(let i = number; i >= 0; i-=5) {
	if (i % 10 === 0 && i > 50) {
		console.log('The number is divisible by 10. Skipping the number.');
		continue;
	}

	if (i % 5 === 0 && i > 50) {
		console.log(i);
	}

	if (i <= 50) {
		console.log('The current value is below or equal to 50. Terminating the loop...');
		break;
	}
} 

console.log('\n\nNumber 2:\n\n')

let word = 'supercalifragilisticexpialidocious';
let consonants = '';

for(i = 0; i < word.length; i++) {
	if (word[i].toLowerCase()==='a'||
			word[i].toLowerCase()==='e'||
			word[i].toLowerCase()==='i'||
			word[i].toLowerCase()==='o'||
			word[i].toLowerCase()==='u'){
	        continue;
	} else {
		consonants += word[i];
	}
}

console.log(consonants);